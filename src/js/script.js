$(document).ready(function() {
  
	// js-edit

	function closeEdit() {
		$('.profile-info__edit-action').removeClass('open');
	};

	function getEdit( $thisEdit ) {
		return $thisEdit.parent('.profile-info__input-wrap').find('.profile-info__edit-value').text();
	};

	function setEdit( $thisEdit, val ) {
		$thisEdit.parent('.profile-info__input-wrap').find('.profile-info__edit-value').text(val);
	};

	// open edit
	$('.profile-info-js-edit').click(function(e) {
		e.preventDefault();
		var $this = $(this).parent('.profile-info__edit');

		// not multiple open
		if ( $($this).hasClass('open') == false ) {
			closeEdit();
			$($this).find('.profile-info__edit-action').addClass('open');
			// get value
			$($this).find('.mdl-textfield__input').val( getEdit($($this)) ).focus();
		}

	});

  // click on close btn
  $('.profile-info-js-edit-close').click(function(e) {
    e.preventDefault();
    var $this = $(this).parent('.profile-info__edit-action').parent('.profile-info__edit');
    // reset to default value
    $($this).find('.mdl-textfield__input').val( getEdit($($this)) );
    closeEdit();
  });

  // click on save btn
  $('.profile-info-js-edit-save').click(function(e) {
    e.preventDefault();
    var $this = $(this).parent('.profile-info__edit-action').parent('.profile-info__edit'),
        val = $($this).find('.mdl-textfield__input').val();
    // send value
    setEdit( $($this), val );
    closeEdit();
  });

  // open mobile edit

  $('.js-edit-mobile').click(function(e) {
    e.preventDefault();
    $(this).parent().addClass('open');
    $('.profile-info__edit-mobile-action').addClass('open');
    $('.profile-about-form').css('display', 'none');
  });

  // close mobile edit
  $('.profile-info__edit-mobile-close').click(function(e) {
    e.preventDefault();
    $('.profile-info__edit-mobile').removeClass('open');
    $('.profile-info__edit-mobile-action').removeClass('open');
    $('.profile-about-form').css('display', 'block');
  });

	/**
   * Tabs
   * @function
   * @param {string} tabs - The selector for tabs.
   * @param {string} tabsToogle - The selector for tabs toogle.
   * @param {string} toogleClass - The class name for active tab and toogle.
   * @param {integer} tabsNumberActive - Active tab number if = 0 - all tabs close.
   * @param {integer} animationSpeed - Animation speed. Animation for CSS using toogleClass.
   */
  function tabs(tabs, tabsToogle, toogleClass, tabsNumberActive, animationSpeed) {
  
    // start TABS
    
    // basic var
    var i = 1,
        tabNumber;

    // elements
    var $tabs = $(tabs),
        $tabsToogle = $(tabsToogle);

    
    $tabs.each(function() {
      if ( tabsNumberActive === 0 ) {
        $(this).css('display', 'none');
      } else {
        if ( i === tabsNumberActive ) {
          $(this).css('display', 'block')
        } else {
          $(this).css('display', 'none');
        }
      }
      $(this).attr( 'data-tab-number', i ); // add tab number
      i++;
    });
    
    i = 1;
    
    $tabsToogle.each(function() {
      if ( tabsNumberActive === 0 ) {
        $(this).removeClass(toogleClass);
      } else {
        if ( i === tabsNumberActive ) {
          $(this).addClass(toogleClass);
        } else {
          $(this).removeClass(toogleClass);
        }
      }
      $(this).attr( 'data-tab-number', i ); // add tab number
      i++;
    });
    
    
    // toogle click
    $tabsToogle.click(function(e) {
        e.preventDefault();
        $tabsToogle.removeClass(toogleClass);
        $(this).addClass(toogleClass);
    
        tabNumber = $(this).attr('data-tab-number');
    
        $tabs.removeClass(toogleClass);
            setTimeout(function() { 
                $tabs.css('display', 'none');
            }, animationSpeed);
    
        $(tabs +'[data-tab-number="'+ tabNumber +'"]').addClass(toogleClass);
            setTimeout(function() {
                $(tabs +'[data-tab-number="'+ tabNumber +'"]').css('display', 'block');
            }, animationSpeed);
    
    }); // /toogle click
  
  }; // /end TABS
  
  // tab init
	tabs('.profile-js-tabs', '.profile-js-control a', 'active-control', 1, 100);

  if ( $(window).width() < 600 ) {
    $('.profile-js-control').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      centerPadding: '30px',
      variableWidth: true
    });
  }

});
